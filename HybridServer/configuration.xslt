<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:c="http://www.esei.uvigo.es/dai/proyecto">
	<xsl:output method="html" indent="yes" encoding="utf-8" />

	<xsl:template match="/">
		<html>
			<head>
				<title>Configuración</title>
			</head>
			<style>
				#body{
					width: 100%
				}
				#cuerpo{
					width: 600px;
					margin: 0px auto;
					padding: 10px 20px;
				}
				body{
					background-color: #EEEEEE;
				
				}
				tr{
				}					
				th{
					text-align: left;
					padding: 5px;
					width: 152px;
					border: 1px solid ;
					color: #515151;
				}
				td{
					color: #515151;
					border: 1px solid ;
				}
				h3{
					text-align: center;
					
				}
				table{
					background-color: #FFFFFF;
					width:100%;
					border-collapse: collapse;
					margin-bottom: 10px;
				}
				h2{
					background-color: #00B4FF;
					padding: 5px;
					margin: 0px;
					margin-top: 5px;
					color:white;
				}
				h1{
					text-align:center;
					color: #515151;
				}
			</style>
			<body>
				<div id="body">
					<div id="cuerpo">
						<h1>Configuración</h1>
						<div id="connection">
							<h2>Conexión</h2>
							<xsl:apply-templates select="c:configuration/c:connections" />
						</div>
						<div id="database">
							<h2>Configuración DataBase</h2>
							<xsl:apply-templates select="c:configuration/c:database" />
						</div>
						<div id="servicios" >
							<h2>Servers</h2>
							<xsl:apply-templates select="c:configuration/c:servers/c:server" />
						</div>
					</div>
				</div>
			</body>
		</html>
	</xsl:template>
	
	<xsl:template match="c:connections">
		<div >
			<table>
				<tr>
					<th>Url: </th>
					<td><xsl:value-of select="c:webservice/text()" /></td>
				</tr>
				<tr>
					<th>Puerto: </th>
					<td><xsl:value-of select="c:http/text()" /></td>
				</tr>
				<tr>
					<th>Numero de Clientes: </th>
					<td><xsl:value-of select="c:numClients/text()" /></td>
				</tr>
			</table>
		</div>

	</xsl:template>
	
	<xsl:template match="c:database">
		<div >
			<table>
				<tr>
					<th>Usuario: </th>
					<td><xsl:value-of select="c:user/text()" /></td>
				</tr>
				<tr>
					<th>Password: </th>
					<td><xsl:value-of select="c:password/text()" /></td>
				</tr>
				<tr>
					<th>Url: </th>
					<td><xsl:value-of select="c:url/text()" /></td>
				</tr>
			</table>
		</div>
	</xsl:template>
	
	<xsl:template match="c:server">
		<div >
			<table>
				<tr >
					<th colspan="2" ><h3><xsl:value-of select="@name" /></h3></th>
				</tr>
				<tr>
					<th>WSDL: </th>
					<td><xsl:value-of select="@wsdl" /></td>
				</tr>
				<tr>
					<th>NameSpaces: </th>
					<td><xsl:value-of select="@namespace" /></td>
				</tr>
				<tr>
					<th>service: </th>
					<td><xsl:value-of select="@service" /></td>
				</tr>
				<tr>
					<th>httpAddress: </th>
					<td><xsl:value-of select="@httpAddress" /></td>
				</tr>
			</table>
		</div>
	</xsl:template>
</xsl:stylesheet>