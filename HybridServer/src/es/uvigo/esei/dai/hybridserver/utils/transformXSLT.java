package es.uvigo.esei.dai.hybridserver.utils;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.xml.sax.SAXException;

import es.uvigo.esei.dai.hybridserver.file.XMLfile;
import es.uvigo.esei.dai.hybridserver.file.XSDfile;
import es.uvigo.esei.dai.hybridserver.file.XSLTfile;
import es.uvigo.esei.dai.hybridserver.http.HTTPResponseStatus;

public class transformXSLT {
	public static String transform(XMLfile xml, XSLTfile xslt, XSDfile xsd) 
			throws TransformerException, ParserConfigurationException, SAXException, IOException{

		DOMParsing.loadAndValidateWithExternalXSDnoFile(xml.getContent(), xsd.getContent());

		
		TransformerFactory tFactory = TransformerFactory.newInstance();
		Transformer transformer = tFactory.newTransformer(new StreamSource( new StringReader(xslt.getContent())));
		
		StringWriter stringWriter = new StringWriter();
		
		transformer.transform(
				new StreamSource( new StringReader(xml.getContent())), 
				new StreamResult(stringWriter)
				);
		
		;
		
		return  stringWriter.toString();
	
	}
}
