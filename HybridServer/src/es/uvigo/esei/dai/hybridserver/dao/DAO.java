package es.uvigo.esei.dai.hybridserver.dao;

/**
 * Interfaz DAO, Define los mentodos a implementar para clases DAO.
 * 
 * @author Javier Lorenzo Martin; Hector Nóvoa Nóvoa
 *
 */

public interface DAO<E> {
	/**
	 * Recupera el valor(String)  asociado a un UUID en la base de información.
	 * @param uuid
	 * @return Si no se encuentra el valor devuelve null.
	 */
	public E get(String uuid);
	/**
	 * Almacena el String value en la base de información. Con una clave UUID asociada.
	 * @param value
	 * @return String con la Key (uuid) asociada, null si no se encuentra.
	 */
	public E create(String value);
	/**
	 * Recupera todos los archivos. 
	 * @return String[] de claves uuid.
	 */
	public E[] list() ;
	/**
	 * Recupera la lista de claves de la base de informacion.
	 * @return Array de uuids
	 */
	public String[] listUUID();
	/**
	 * Elimina el elemento asociado al uuid parado como parametro.
	 * @param uuid
	 */
	public void delete(String uuid) ;
	/**
	 * Consulta si la base de información esta vacia.
	 * @return boolean
	 */
	public boolean isEmpty();
}
