package es.uvigo.esei.dai.hybridserver.dao;

import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import es.uvigo.esei.dai.hybridserver.Configuration;
import es.uvigo.esei.dai.hybridserver.file.HTMLfile;
import es.uvigo.esei.dai.hybridserver.file.XMLfile;

/**
 * Inmplementa la interfaz DAO, para bases de datos SQL.
 * 
 * @author Javier Lorenzo Martin; Hector Nóvoa Nóvoa
 *
 */
public class DBhtmlDAO implements DAO<HTMLfile> {
	private Connection connection;
	/**
	 * Recibe la conexión a la base de datos.
	 * @param connection
	 */
	public DBhtmlDAO(Connection connection) {
		this.connection = connection;
	}
	
	public DBhtmlDAO(Configuration configuration) throws SQLException {
		this.connection = DriverManager.getConnection(configuration.getDbURL(), configuration.getDbUser(), configuration.getDbPassword()); 
	}
	
	@Override
	public HTMLfile get(String uuid) {
		String sql = "SELECT * FROM HTML WHERE uuid = ?";
		try(PreparedStatement statement = connection.prepareStatement(sql)){
			statement.setString(1, uuid);
			ResultSet r = statement.executeQuery();
			if(r.next()){
				HTMLfile file = new HTMLfile();
				file.setContent(r.getString("content"));
				file.setUuid(r.getString("uuid"));
				return file;
			}
			else 
				return null;
			
		}catch(SQLException e){
			throw new RuntimeException(e);
		}
	}

	@Override
	public HTMLfile create(String value) {

		String sql = "INSERT INTO HTML (uuid, content) VALUES ( ?, ?)";
		
		try(PreparedStatement statement = connection.prepareStatement(sql,Statement.RETURN_GENERATED_KEYS)){
			String uuid =  java.util.UUID.randomUUID().toString() ;
			statement.setString(1,uuid);
			statement.setString(2, value );
			
			if (statement.executeUpdate() == 1){
				return new HTMLfile(value, uuid);
			}else
				throw new SQLException("Resultado inesperado");
		}catch(SQLException e){
			throw new RuntimeException(e);
		}
	}

	@Override
	public HTMLfile[] list() {
		ArrayList<HTMLfile> lista = new ArrayList<HTMLfile>();
		String sql = "SELECT uuid, content FROM HTML";
				
		try(PreparedStatement statement = connection.prepareStatement(sql)){
			 ResultSet r = statement.executeQuery();
			 while(r.next()){
				 lista.add(new HTMLfile(r.getString(2), r.getString(1)));
			 }
		}catch(SQLException e){
			throw new RuntimeException(e);
		} 
			
		HTMLfile[] respuesta = new HTMLfile[lista.size()];
		return lista.toArray(respuesta);
	}

	@Override
	public void delete(String uuid) {

		String sql = "DELETE FROM HTML WHERE uuid = ?";
		
		try(PreparedStatement statement = this.connection.prepareStatement(sql)){
			
			statement.setString(1, uuid);
			
			int res = statement.executeUpdate() ;
			if (res== 0){
				try {
					throw new FileNotFoundException("Pagina no existente");
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
		}catch(SQLException e){
			throw new RuntimeException();
		}
	}

	@Override
	public boolean isEmpty() {
		String sql = "SELECT uuid FROM HTML";
				
		try(PreparedStatement statement = this.connection.prepareStatement(sql)){
			 ResultSet r = statement.executeQuery();
			 if(r.next()){
				 return false;
			 }
		}catch(SQLException e){
			throw new RuntimeException(e);
		} 
		return true;
	}

	@Override
	public String[] listUUID() {
		ArrayList<String> lista = new ArrayList<String>();
		String sql = "SELECT uuid FROM HTML";
				
		try(PreparedStatement statement = connection.prepareStatement(sql)){
			 ResultSet r = statement.executeQuery();
			 while(r.next()){
				 lista.add(r.getString(1));
			 }
		}catch(SQLException e){
			throw new RuntimeException(e);
		} 
			
		return lista.toArray(new String[lista.size()]);
	}

}
