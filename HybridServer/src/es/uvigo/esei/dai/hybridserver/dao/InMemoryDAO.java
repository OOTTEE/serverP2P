package es.uvigo.esei.dai.hybridserver.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import es.uvigo.esei.dai.hybridserver.file.HTMLfile;

/**
 * 
 * 
 * @author Javier Lorenzo Martin; Hector Nóvoa Nóvoa
 *
 */

public class InMemoryDAO implements DAO<HTMLfile> {
	private Map<String,String> biblioteca;

	public InMemoryDAO() {
		this.biblioteca = new HashMap<String, String> ();
	}
	
	public InMemoryDAO(Map <String,String> biblioteca) {
		this.biblioteca = new HashMap<String, String> ();
		this.biblioteca.putAll(biblioteca);
	}
	@Override
	public HTMLfile get(String uuid) {
		return  (this.biblioteca.get(uuid) != null) ? new HTMLfile(this.biblioteca.get(uuid), uuid) : null;
	}	
	@Override
	public HTMLfile create(String value) {
		String uuid = java.util.UUID.randomUUID().toString();
		this.biblioteca.put(uuid, value);
		return new HTMLfile(value, uuid);
	}	
	@Override
	public HTMLfile[] list() {
		HTMLfile[] aux = new HTMLfile[this.biblioteca.size()];
		int i =0;
		for( Map.Entry<String,String> param : this.biblioteca.entrySet()){
			aux[i]= new HTMLfile(param.getValue(), param.getKey());
			i++;
		}
		return aux;
	}	
	@Override
	public void delete(String uuid) {
		this.biblioteca.remove(uuid);
	}	
	@Override	
	public boolean isEmpty(){
		return this.biblioteca.isEmpty();
	}
	@Override
	public String[] listUUID() {
		String[] aux = new String[this.biblioteca.size()];
		int i =0;
		for( Map.Entry<String,String> param : this.biblioteca.entrySet()){
			aux[i]= param.getValue();
			i++;
		}
		return aux;
	}

	
	
	
}
