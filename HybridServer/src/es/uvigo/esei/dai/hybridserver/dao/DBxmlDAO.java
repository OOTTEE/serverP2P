package es.uvigo.esei.dai.hybridserver.dao;

import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import es.uvigo.esei.dai.hybridserver.Configuration;
import es.uvigo.esei.dai.hybridserver.file.HTMLfile;
import es.uvigo.esei.dai.hybridserver.file.XMLfile;


public class DBxmlDAO implements DAO<XMLfile> {
	private Connection connection;
	public DBxmlDAO(Connection conexion) {
		this.connection = conexion;
	}
	public DBxmlDAO(Configuration configuration) throws SQLException {
		this.connection = DriverManager.getConnection(configuration.getDbURL(), configuration.getDbUser(), configuration.getDbPassword()); 
	}
	
	@Override
	public XMLfile get(String uuid) {
		String sql = "SELECT * FROM XML WHERE uuid = ?";
		try(PreparedStatement statement = connection.prepareStatement(sql)){
			statement.setString(1, uuid);
			ResultSet r = statement.executeQuery();
			if(r.next()){
				XMLfile file = new XMLfile();
				file.setContent(r.getString("content"));
				file.setUuid(r.getString("uuid"));
				return file;
			}
			else 
				return null;
			
		}catch(SQLException e){
			throw new RuntimeException(e);
		}
	}

	@Override
	public XMLfile create(String value) {

		String sql = "INSERT INTO XML (uuid, content) VALUES ( ?, ?)";
		
		try(PreparedStatement statement = connection.prepareStatement(sql,Statement.RETURN_GENERATED_KEYS)){
			String uuid =  java.util.UUID.randomUUID().toString() ;
			statement.setString(1,uuid);
			statement.setString(2, value );
			
			if (statement.executeUpdate() == 1){
				return new XMLfile(value, uuid);
			}else
				throw new SQLException("Resultado inesperado");
		}catch(SQLException e){
			throw new RuntimeException(e);
		}
	}

	@Override
	public XMLfile[] list() {
		ArrayList<XMLfile> lista = new ArrayList<XMLfile>();
		String sql = "SELECT uuid, content FROM XML";
				
		try(PreparedStatement statement = connection.prepareStatement(sql)){
			 ResultSet r = statement.executeQuery();
			 while(r.next()){
				 lista.add(new XMLfile(r.getString(2), r.getString(1)));
			 }
		}catch(SQLException e){
			throw new RuntimeException(e);
		} 
			
		XMLfile[] respuesta = new XMLfile[lista.size()];
		return lista.toArray(respuesta);
	}

	@Override
	public void delete(String uuid) {
		String sql = "DELETE FROM XML WHERE uuid = ?";
		
		try(PreparedStatement statement = connection.prepareStatement(sql)){
			
			statement.setString(1, uuid);
			
			int res = statement.executeUpdate() ;
			if (res== 0){
				try {
					throw new FileNotFoundException("Pagina no existente");
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
		}catch(SQLException e){
			throw new RuntimeException();
		}
	}

	@Override
	public boolean isEmpty() {
		String sql = "SELECT uuid FROM XML";
				
		try(PreparedStatement statement = connection.prepareStatement(sql)){
			 ResultSet r = statement.executeQuery();
			 if(r.next()){
				 return false;
			 }
		}catch(SQLException e){
			throw new RuntimeException(e);
		} 
		return true;
	}
	@Override
	public String[] listUUID() {
		ArrayList<String> lista = new ArrayList<String>();
		String sql = "SELECT uuid FROM XML";
				
		try(PreparedStatement statement = connection.prepareStatement(sql)){
			 ResultSet r = statement.executeQuery();
			 while(r.next()){
				 lista.add(r.getString(1));
			 }
		}catch(SQLException e){
			throw new RuntimeException(e);
		} 
			
		return lista.toArray(new String[lista.size()]);
	}

}
