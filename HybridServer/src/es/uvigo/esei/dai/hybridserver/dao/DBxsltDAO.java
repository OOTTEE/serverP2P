package es.uvigo.esei.dai.hybridserver.dao;

import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import es.uvigo.esei.dai.hybridserver.Configuration;
import es.uvigo.esei.dai.hybridserver.file.XSDfile;
import es.uvigo.esei.dai.hybridserver.file.XSLTfile;

public class DBxsltDAO implements DAO<XSLTfile> {

	private Connection connection;
	public DBxsltDAO(Connection conexion) {
		this.connection = conexion;
	}
	
	public DBxsltDAO(Configuration configuration) throws SQLException {
		this.connection = DriverManager.getConnection(configuration.getDbURL(), configuration.getDbUser(), configuration.getDbPassword()); 
	}
	
	
	@Override
	public XSLTfile get(String uuid) {
		String sql = "SELECT * FROM XSLT WHERE uuid = ?";
		try(PreparedStatement statement = connection.prepareStatement(sql)){
			statement.setString(1, uuid);
			ResultSet r = statement.executeQuery();
			if(r.next()){
				return new XSLTfile(r.getString("content"),r.getString("uuid"),r.getString("xsd"));
			}
			else 
				return null;
		}catch(SQLException e){
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public XSLTfile create(String value) {

		String sql = "INSERT INTO XSLT (uuid, content, xsd) VALUES ( ?, ?, ?)";
		
		try(PreparedStatement statement = connection.prepareStatement(sql,Statement.RETURN_GENERATED_KEYS)){
			String uuid =  java.util.UUID.randomUUID().toString() ;
			statement.setString(1,uuid);
			statement.setString(2,value);
			statement.setString(3,"");
			
			if (statement.executeUpdate() == 1){
				return new XSLTfile(value, uuid);
			}else
				throw new SQLException("Resultado inesperado");
		}catch(SQLException e){
			throw new RuntimeException(e);
		}
	}
	
	public XSLTfile create(String value, String xsd) {

		String sql = "INSERT INTO XSLT (uuid, content, xsd) VALUES ( ?, ?, ?)";
		
		try(PreparedStatement statement = connection.prepareStatement(sql,Statement.RETURN_GENERATED_KEYS)){
			String uuid =  java.util.UUID.randomUUID().toString() ;
			statement.setString(1,uuid);
			statement.setString(2,value);
			statement.setString(3,xsd);
			
			if (statement.executeUpdate() == 1){
				return new XSLTfile(value, uuid, xsd);
			}else
				throw new SQLException("Resultado inesperado");
		}catch(SQLException e){
			throw new RuntimeException(e);
		}
	}

	@Override
	public XSLTfile[] list() {
		ArrayList<XSLTfile> lista = new ArrayList<XSLTfile>();
		String sql = "SELECT uuid, content FROM XSLT";
				
		try(PreparedStatement statement = connection.prepareStatement(sql)){
			 ResultSet r = statement.executeQuery();
			 while(r.next()){
				 lista.add(new XSLTfile(r.getString(2), r.getString(1)));
			 }
		}catch(SQLException e){
			throw new RuntimeException(e);
		} 
			
		XSLTfile[] respuesta = new XSLTfile[lista.size()];
		return lista.toArray(respuesta);
	}

	@Override
	public void delete(String uuid) {
		String sql = "DELETE FROM XSLT WHERE uuid = ?";
		
		try(PreparedStatement statement = connection.prepareStatement(sql)){
			
			statement.setString(1, uuid);
			
			int res = statement.executeUpdate() ;
			if (res== 0){
				try {
					throw new FileNotFoundException("Pagina no existente");
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
		}catch(SQLException e){
			throw new RuntimeException();
		}
	}

	@Override
	public boolean isEmpty() {
		String sql = "SELECT uuid FROM XSLT";
				
		try(PreparedStatement statement = connection.prepareStatement(sql)){
			 ResultSet r = statement.executeQuery();
			 if(r.next()){
				 return false;
			 }
		}catch(SQLException e){
			throw new RuntimeException(e);
		} 
		return true;
	}
	@Override
	public String[] listUUID() {
		ArrayList<String> lista = new ArrayList<String>();
		String sql = "SELECT uuid FROM XSLT";
				
		try(PreparedStatement statement = connection.prepareStatement(sql)){
			 ResultSet r = statement.executeQuery();
			 while(r.next()){
				 lista.add(r.getString(1));
			 }
		}catch(SQLException e){
			throw new RuntimeException(e);
		} 
			
		return lista.toArray(new String[lista.size()]);
	}

}
