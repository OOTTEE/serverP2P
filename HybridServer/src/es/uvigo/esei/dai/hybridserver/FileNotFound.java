package es.uvigo.esei.dai.hybridserver;

public class FileNotFound extends RuntimeException {

	public FileNotFound() {
	}

	public FileNotFound(String message) {
		super(message);
	}

	public FileNotFound(Throwable cause) {
		super(cause);
	}

	public FileNotFound(String message, Throwable cause) {
		super(message, cause);
	}

	public FileNotFound(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
