package es.uvigo.esei.dai.hybridserver;

import java.sql.SQLException;

import javax.jws.WebService;

import es.uvigo.esei.dai.hybridserver.dao.DBhtmlDAO;
import es.uvigo.esei.dai.hybridserver.dao.DBxmlDAO;
import es.uvigo.esei.dai.hybridserver.dao.DBxsdDAO;
import es.uvigo.esei.dai.hybridserver.dao.DBxsltDAO;
import es.uvigo.esei.dai.hybridserver.file.HTMLfile;
import es.uvigo.esei.dai.hybridserver.file.XMLfile;
import es.uvigo.esei.dai.hybridserver.file.XSDfile;
import es.uvigo.esei.dai.hybridserver.file.XSLTfile;
import es.uvigo.esei.dai.hybridserver.xmlserver.controllers.ServiceException;

@WebService(
		endpointInterface = "es.uvigo.esei.dai.hybridserver.ServicioServer",
		serviceName= "HybridServerService"		
	)
public class ServicioServerImpl implements ServicioServer{

	Configuration configuration;
	
	public ServicioServerImpl(Configuration configuration) {
		this.configuration = configuration;
	}
	
	@Override
	public String[] getHTMLList() {
		DBhtmlDAO BD;
		try {
			BD = new DBhtmlDAO(this.configuration);
		} catch (SQLException e) {
			throw new ServiceException(e.getMessage());
		} 
		
		return BD.listUUID();
	}

	@Override
	public HTMLfile getHTML(String uuid) {
		DBhtmlDAO BD;
		try {
			BD = new DBhtmlDAO(this.configuration);
		} catch (SQLException e) {
			throw new ServiceException(e.getMessage());
		} 
		
		return BD.get(uuid);
	}

	@Override
	public String[] getXMLList() {
		DBxmlDAO BD;
		try {
			BD = new DBxmlDAO(this.configuration);
		} catch (SQLException e) {
			throw new ServiceException(e.getMessage());
		} 
		
		return BD.listUUID();
	}

	@Override
	public XMLfile getXML(String uuid) {
		DBxmlDAO BD;
		try {
			BD = new DBxmlDAO(this.configuration);
		} catch (SQLException e) {
			throw new ServiceException(e.getMessage());
		} 
		
		return BD.get(uuid);
	}

	@Override
	public String[] getXSLTList() {
		DBxsltDAO BD;
		try {
			BD = new DBxsltDAO(this.configuration);
		} catch (SQLException e) {
			throw new ServiceException(e.getMessage());
		} 
		
		return BD.listUUID();
	}

	@Override
	public XSLTfile getXSLT(String uuid) {
		DBxsltDAO BD;
		try {
			BD = new DBxsltDAO(this.configuration);
		} catch (SQLException e) {
			throw new ServiceException(e.getMessage());
		} 
		
		return BD.get(uuid);
	}

	@Override
	public String[] getXSDList() {
		DBxsdDAO BD;
		try {
			BD = new DBxsdDAO(this.configuration);
		} catch (SQLException e) {
			throw new ServiceException(e.getMessage());
		} 
		
		return BD.listUUID();
	}

	@Override
	public XSDfile getXSD(String uuid) {
		DBxsdDAO BD;
		try {
			BD = new DBxsdDAO(this.configuration);
		} catch (SQLException e) {
			throw new ServiceException(e.getMessage());
		} 
		
		return BD.get(uuid);
	}
}
