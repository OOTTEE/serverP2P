package es.uvigo.esei.dai.hybridserver.file;

public interface File {
	public String getContent();
	public void setContent(String html);
	public String getUuid();
	public void setUuid(String uuid);
}
