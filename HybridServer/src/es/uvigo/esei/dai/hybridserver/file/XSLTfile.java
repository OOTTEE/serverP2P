package es.uvigo.esei.dai.hybridserver.file;

public class XSLTfile implements File {
		private String uuid;
		private String content;
		private String xsd;
		public XSLTfile() {
			// TODO Auto-generated constructor stub
		}
		public XSLTfile(String html, String uuid, String xsd) {
			this.content = html;
			this.uuid = uuid;
			this.xsd = xsd;
		}
		public XSLTfile(String html, String uuid) {
			this.content = html;
			this.uuid = uuid;
		}
		@Override
		public String getContent() {
			// TODO Auto-generated method stub
			return this.content;
		}

		@Override
		public void setContent(String content) {
			// TODO Auto-generated method stub
			this.content = content;
		}

		@Override
		public String getUuid() {
			// TODO Auto-generated method stub
			return this.uuid;
		}
		@Override
		public void setUuid(String uuid) {
			// TODO Auto-generated method stub
			this.uuid = uuid;

		}

		public String getXsd() {
			// TODO Auto-generated method stub
			return this.xsd;
		}
		
		public void setXsd(String xsd) {
			// TODO Auto-generated method stub
			this.xsd = xsd;

		}
}