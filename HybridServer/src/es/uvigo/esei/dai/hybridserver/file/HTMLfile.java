package es.uvigo.esei.dai.hybridserver.file;

import java.sql.ResultSet;
import java.sql.SQLException;

public class HTMLfile implements File{
	private String content;
	private String uuid;

	public HTMLfile(String html, String uuid) {
		this.content = html;
		this.uuid = uuid;
	}
	public HTMLfile(){}
	
	public String getContent() {
		return content;
	}
	public void setContent(String html) {
		this.content = html;
	}
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

}
