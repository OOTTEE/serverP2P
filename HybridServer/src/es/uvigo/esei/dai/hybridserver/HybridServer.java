package es.uvigo.esei.dai.hybridserver;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
//import java.util.Map;
import java.util.Properties;

import javax.xml.ws.Endpoint;

import es.uvigo.esei.dai.hybridserver.dao.DAO;
import es.uvigo.esei.dai.hybridserver.dao.InMemoryDAO;

//import es.uvigo.esei.dai.hybridserver.dao.DAO;

/**
 * Clase Lanucher del HybridServer.
 * 
 * @author Javier Lorenzo Martin; Hector Nóvoa Nóvoa
 *
 */


public class HybridServer  {
	
	private boolean running;
	private ListenThread listen;
	private DAO biblioteca;
	private Configuration configuration;
	private Endpoint servicio;
	/**
	 * Constructor, importa una biblioteca como un Map de datos.
	 * y carga el archivo de propiedades.
	 * @param biblioteca Contiene una biblioteca de informacion (clave<String>,Valor<String>)
	 */
	public HybridServer(Map <String,String> biblioteca ){
		this.biblioteca = new InMemoryDAO(biblioteca);
		this.configuration = new Configuration(
				2000,
				50,
				null,
				null,
				null,
				null,
				null
				);
	}
	
	public HybridServer(){
		this.biblioteca = new InMemoryDAO(new HashMap<String,String>());
		this.configuration = new Configuration(
				2000,
				50,
				null,
				null,
				null,
				null,
				null
				);
		
	}
	
	/**
	 * Constructor que importa las propiedades del server a traves de un parametro
 	 * y crea la conexion a la base de datos con los datos cargados a traves de propierties.
	 * @param prop Propiedades del servidor.
	 * @throws SQLException
	 */
	public HybridServer(Properties prop) throws SQLException{
		this.configuration = new Configuration(
				Integer.parseInt(prop.getProperty("port")),
				Integer.parseInt(prop.getProperty("numClients")),
				null,
				prop.getProperty("db.user"),
				prop.getProperty("db.password"),
				prop.getProperty("db.url"),
				null
				);
	}

	/**
	 * Constructor que importa las propiedades del server a traves de un parametro
 	 * y crea la conexion a la base de datos con los datos cargados a traves de propierties.
	 * @param prop Propiedades del servidor.
	 * @throws SQLException
	 */
	public HybridServer(Configuration conf) throws SQLException{
		this.configuration = conf;
	}
	/**
	 * Inicia el servidor.
	 * Se crea un hilo nuevos que estará pendiente de las nuevas peticiones. 
	 * Los parametros necesarios para el hilos son (puerto de conexion, biblioteca DAO, numero maximo de clientes simultaneos) 
	 */
	public void start() {
		if(!this.running){
			this.running = true;
			try {
				this.listen = new ListenThread(this.configuration);
			} catch (IOException e) {
				e.printStackTrace();
			}
			this.listen.start();
			if(this.configuration.getWebServiceURL() != null)
				launchService();
		}
	}
	
	/**
	 * Termina la ejecucion del servidor.
	 */
	public void stop(){
		if(this.running){
			this.running = false;
			try {
				this.listen.setStop();
			} catch (IOException e) {
				e.printStackTrace();
			}
			if( servicio != null){
				System.out.println("stop service");
				this.servicio.stop();
			}
		}
	}

	/**
	 * Devuelve un entero con el puerto de escucha
	 * @return
	 */
	public int getPort(){
		return this.configuration.getHttpPort();
	}
	/**
	 * Establece el puerto de escucha a traves de un entero recivido como parametro.
	 * @param port Numero de puerto.
	 */
	public void setPort( int port){
		this.configuration.setHttpPort(port);
	}
	
	private void launchService(){
		this.servicio = Endpoint.publish(
				this.configuration.getWebServiceURL(), 
				new ServicioServerImpl(this.configuration)
			);
	}



}
