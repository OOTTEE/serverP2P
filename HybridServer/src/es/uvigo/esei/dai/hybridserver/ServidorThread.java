package es.uvigo.esei.dai.hybridserver;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

import es.uvigo.esei.dai.hybridserver.dao.DAO;
import es.uvigo.esei.dai.hybridserver.enrutador.Enrutador;
import es.uvigo.esei.dai.hybridserver.http.HTTPParseException;
import es.uvigo.esei.dai.hybridserver.http.HTTPRequest;

/**
 * ServidorThread implementa la ejecucion de los hilos  
 * @author Javier Lorenzo Martin; Hector Nóvoa Nóvoa
 *
 */

public class ServidorThread <E> implements Runnable {

	private Socket so;
	private Connection conexion;
	private List<ServerConfiguration> serverConfiguracion;

	/**
	 * Constructor, Recive el Socket de conexión y El DAO de la base de información
	 * @param soc
	 * @param biblioteca
	 */
	public ServidorThread( Socket soc, DAO<E> biblioteca){
		this.so = soc;
	}
	
	/**
	 * Constructor, Recive el Socket de conexión y El DAO de la base de información
	 * @param soc
	 * @param biblioteca
	 * @throws SQLException 
	 */
	public ServidorThread( Socket soc, Configuration configuration){
		this.so = soc;
		this.serverConfiguracion = configuration.getServers();
		try {
			this.conexion = DriverManager.getConnection(configuration.getDbURL(), configuration.getDbUser(), configuration.getDbPassword());
		}  catch (SQLException e ) {
			System.err.println("ServidorThread SQLException: "+e.getMessage());
			System.err.println("No se pudo conectar a la base de datos.");
		}
	}
	
	/**
	 * Proceso de ejecución del hilo, 
	 */
	@Override
	public void run() {
		try (Socket so = this.so){
			//Se crea el request del stream de entrada
			HTTPRequest request = new HTTPRequest(new InputStreamReader(so.getInputStream()));
			//Creamos un nuevo enrutador con acceso a la base de informacion
			try (Enrutador HC = new Enrutador(this.conexion, this.serverConfiguracion)){
				//Se procresa el request generar el nuevo response y transferirlo por el socket de salida.
				HC.process(request).print(new OutputStreamWriter(so.getOutputStream()));
			}
		} catch (IOException | HTTPParseException | SQLException e) {
			// TODO Auto-generated catch block
			System.err.println("Thread: " + e.getMessage());
		}
		
	}
	

}
