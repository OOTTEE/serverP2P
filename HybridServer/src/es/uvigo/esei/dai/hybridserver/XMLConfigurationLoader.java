/**
 *  HybridServer
 *  Copyright (C) 2014 Miguel Reboiro-Jato
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uvigo.esei.dai.hybridserver;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import es.uvigo.esei.dai.hybridserver.utils.DOMParsing;

public class XMLConfigurationLoader {
	public Configuration load(File xmlFile)	throws Exception {
		
			Document document = DOMParsing.loadAndValidateWithInternalXSD(xmlFile.getAbsolutePath());
			
			Element connection = ((Element) document.getElementsByTagName("connections").item(0));
			Element database = ((Element) document.getElementsByTagName("database").item(0));

			List<ServerConfiguration> servers = new ArrayList<ServerConfiguration>();
			
			
			Configuration conf = new Configuration(
					Integer.parseInt(connection.getElementsByTagName("http").item(0).getTextContent()),
					Integer.parseInt(connection.getElementsByTagName("numClients").item(0).getTextContent()),
					connection.getElementsByTagName("webservice").item(0).getTextContent(),
					database.getElementsByTagName("user").item(0).getTextContent(),
					database.getElementsByTagName("password").item(0).getTextContent(),
					database.getElementsByTagName("url").item(0).getTextContent(),
					servers
					);
			
			NodeList serversNodes = document.getElementsByTagName("server");

			for(int i = 0 ; i < serversNodes.getLength(); i++){
				Element server = (Element) serversNodes.item(i);
				servers.add(new ServerConfiguration(
						server.getAttribute("name"),
						server.getAttribute("wsdl"),
						server.getAttribute("namespace"),
						server.getAttribute("service"),
						server.getAttribute("httpAddress")
						));
			}
			
			return conf;
		
	}
}
