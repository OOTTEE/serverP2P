package es.uvigo.esei.dai.hybridserver;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.Properties;

public class Launcher {
	
	/**
	 * Main del HybridServer.
	 * Crea un nuevo servidor y lo inicia. Usa el constructor por defecto, por lo que se conectara a la base de datos indicada en server.propierties .
	 * Para terminar la ejecución, hay que escribir 'q' o 'Q'
	 * @param args None
	 * @throws SQLException 
	 */
	public static void main(String args[]) throws SQLException{
		if(args.length == 1){
			try{	
				//Creacion del server
				
				Configuration conf = new XMLConfigurationLoader().load(new File(args[0]));
				
				HybridServer h = new HybridServer(conf);
				
				//Incio del server
				h.start();
				
				//Esperamos al fin del programa
				BufferedReader lector = new BufferedReader( new InputStreamReader(System.in));
				String str;
				
				System.out.print("Escriba 'q' para salir: ");
				while(!((str = lector.readLine()).equals("q") || str.equals("Q")))
					System.out.print("Escriba 'q' para salir: ");
				
				//Paramos el server
				h.stop();
				
				
				
			} catch (Exception e) {
				System.err.println("Error al arrancar el servidor: \n\t" + e.getMessage());
			}
		}else
			System.err.println("Falta el archivo de configuracion, añadalo en el argumento 1 de la ejecución");
	}	
	
	
	/**
	 * Carga las propiedades del servidor a traves del archivo server.propierties .
	 */
	private static Properties getProperties(String file){
		Properties prop = new Properties();
		try (InputStream input = new FileInputStream(file)){
			prop.load(input);	 
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return prop;
	}
	
	/**
	 * Main del HybridServer.
	 * Crea un nuevo servidor y lo inicia. Usa el constructor por defecto, por lo que se conectara a la base de datos indicada en server.propierties .
	 * Para terminar la ejecución, hay que escribir 'q' o 'Q'
	 * @param args None
	 * @throws SQLException 
	 */
//	public static void main(String args[]) throws SQLException{
//		if(args.length == 1){
//			try{	
//				//Creacion del server
//				HybridServer h = new HybridServer(getProperties(args[0]));
//				
//				//Incio del server
//				h.start();
//				
//				//Esperamos al fin del programa
//				BufferedReader lector = new BufferedReader( new InputStreamReader(System.in));
//				String str;
//				
//				System.out.print("Escriba 'q' para salir: ");
//				while(!((str = lector.readLine()).equals("q") || str.equals("Q")))
//					System.out.print("Escriba 'q' para salir: ");
//				
//				//Paramos el server
//				h.stop();
//			} catch (IOException e) {
//				System.err.println(e.getMessage());
//			}
//		}else
//			System.err.println("Falta el archivo de configuracion, añadalo en el argumento 1 de la ejecución");
//	}
	

	/**
	 * Main del HybridServer.
	 * Crea un nuevo servidor y lo inicia. El constructor HybridServer recibe un Map de información, que almacenara en memoria.
	 * Para terminar la ejecución, hay que escribir 'q' o 'Q'
	 * @param args
	 */
//	public static void main(String args[]){
//		try{
//			String [][] pages = new String[][] {
//			//  { "uuid",                                 "texto contenido por la página"                               }
//			    { "6df1047e-cf19-4a83-8cf3-38f5e53f7725", "This is the html page 6df1047e-cf19-4a83-8cf3-38f5e53f7725." },
//			    { "79e01232-5ea4-41c8-9331-1c1880a1d3c2", "This is the html page 79e01232-5ea4-41c8-9331-1c1880a1d3c2." },
//			    { "a35b6c5e-22d6-4707-98b4-462482e26c9e", "This is the html page a35b6c5e-22d6-4707-98b4-462482e26c9e." },
//			    { "3aff2f9c-0c7f-4630-99ad-27a0cf1af137", "This is the html page 3aff2f9c-0c7f-4630-99ad-27a0cf1af137." },
//			    { "77ec1d68-84e1-40f4-be8e-066e02f4e373", "This is the html page 77ec1d68-84e1-40f4-be8e-066e02f4e373." },
//			    { "8f824126-0bd1-4074-b88e-c0b59d3e67a3", "This is the html page 8f824126-0bd1-4074-b88e-c0b59d3e67a3." },
//			    { "c6c80c75-b335-4f68-b7a7-59434413ce6c", "This is the html page c6c80c75-b335-4f68-b7a7-59434413ce6c." },
//			    { "f959ecb3-6382-4ae5-9325-8fcbc068e446", "This is the html page f959ecb3-6382-4ae5-9325-8fcbc068e446." },
//			    { "2471caa8-e8df-44d6-94f2-7752a74f6819", "This is the html page 2471caa8-e8df-44d6-94f2-7752a74f6819." },
//			    { "fa0979ca-2734-41f7-84c5-e40e0886e408", "This is the html page fa0979ca-2734-41f7-84c5-e40e0886e408." }
//			};
//			
//			// Creación del servidor con las páginas ya en memoria.
//			final Map<String, String> MapPages = new HashMap<>();
//			for (String[] page : pages) {
//				MapPages.put(page[0], page[1]);
//			}
//	
//			HybridServer h = new HybridServer(MapPages);
//			
//			BufferedReader lector = new BufferedReader( new InputStreamReader(System.in));
//			String str;
//			
//
//			System.out.print("Escriba 'q' para salir: ");
//			while(!((str = lector.readLine()).equals("q") || str.equals("Q")))
//				System.out.print("Escriba 'q' para salir: ");
//
//			
//			h.stop();
//		} catch (IOException e) {
//			System.err.println(e.getMessage());
//		}
//	}

}
