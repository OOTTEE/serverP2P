package es.uvigo.esei.dai.hybridserver.xmlserver.controllers;


import java.sql.SQLException;

import javax.jws.WebService;

import es.uvigo.esei.dai.hybridserver.Configuration;
import es.uvigo.esei.dai.hybridserver.dao.DBhtmlDAO;
import es.uvigo.esei.dai.hybridserver.dao.DBxmlDAO;
import es.uvigo.esei.dai.hybridserver.dao.DBxsdDAO;
import es.uvigo.esei.dai.hybridserver.dao.DBxsltDAO;
import es.uvigo.esei.dai.hybridserver.file.HTMLfile;
import es.uvigo.esei.dai.hybridserver.file.XMLfile;
import es.uvigo.esei.dai.hybridserver.file.XSDfile;
import es.uvigo.esei.dai.hybridserver.file.XSLTfile;


@WebService(
	endpointInterface = "es.uvigo.esei.dai.xmlserver.controllers.ControllerInterface"
)
public class Controller implements ControllerInterface {

	private Configuration configuration;
	
	public Controller(Configuration c) {
		this.configuration = c;
	}
	
	@Override
	public String[] getHTMLList() {
		DBhtmlDAO BD;
		try {
			BD = new DBhtmlDAO(this.configuration);
		} catch (SQLException e) {
			throw new ServiceException(e.getMessage());
		} 
		
		return BD.listUUID();
	}

	@Override
	public HTMLfile getHTML(String uuid) {
		DBhtmlDAO BD;
		try {
			BD = new DBhtmlDAO(this.configuration);
		} catch (SQLException e) {
			throw new ServiceException(e.getMessage());
		} 
		
		return BD.get(uuid);
	}

	@Override
	public String[] getXMlList() {
		DBxmlDAO BD;
		try {
			BD = new DBxmlDAO(this.configuration);
		} catch (SQLException e) {
			throw new ServiceException(e.getMessage());
		} 
		
		return BD.listUUID();
	}

	@Override
	public XMLfile getXML(String uuid) {
		DBxmlDAO BD;
		try {
			BD = new DBxmlDAO(this.configuration);
		} catch (SQLException e) {
			throw new ServiceException(e.getMessage());
		} 
		
		return BD.get(uuid);
	}

	@Override
	public String[] getXSLTList() {
		DBxsltDAO BD;
		try {
			BD = new DBxsltDAO(this.configuration);
		} catch (SQLException e) {
			throw new ServiceException(e.getMessage());
		} 
		
		return BD.listUUID();
	}

	@Override
	public XSLTfile getXSLT(String uuid) {
		DBxsltDAO BD;
		try {
			BD = new DBxsltDAO(this.configuration);
		} catch (SQLException e) {
			throw new ServiceException(e.getMessage());
		} 
		
		return BD.get(uuid);
	}

	@Override
	public String[] getXSDList() {
		DBxsdDAO BD;
		try {
			BD = new DBxsdDAO(this.configuration);
		} catch (SQLException e) {
			throw new ServiceException(e.getMessage());
		} 
		
		return BD.listUUID();
	}

	@Override
	public XSDfile getXSD(String uuid) {
		DBxsdDAO BD;
		try {
			BD = new DBxsdDAO(this.configuration);
		} catch (SQLException e) {
			throw new ServiceException(e.getMessage());
		} 
		
		return BD.get(uuid);
	}

}
