package es.uvigo.esei.dai.hybridserver.xmlserver.controllers;

public class ServiceException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	private final String faultInfo;
	
	public ServiceException(String message){
		this(message, message);
	}
	
	public ServiceException(String message, String faultInfo) {
		super(message);
		this.faultInfo=faultInfo;
	}
	
	public String getFaultInfo(){
		return faultInfo;
	}

}
