package es.uvigo.esei.dai.hybridserver.xmlserver.controllers;

import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;

import es.uvigo.esei.dai.hybridserver.HybridServer;
import es.uvigo.esei.dai.hybridserver.ServicioServer;
import es.uvigo.esei.dai.hybridserver.file.HTMLfile;

public class clientHybrid {
	public static void main(String[] args) throws MalformedURLException {
		
		Service service = Service.create(
				new URL("http://localhost:20001/hybridserver?wsdl"),
				new QName(
					"http://hybridserver.dai.esei.uvigo.es/",
					"HybridServerService"
				));
		
		ServicioServer  serviceHybridServer = service.getPort(ServicioServer.class);
		
		String[] files = serviceHybridServer.getHTMLList();
		
		for(int i = 0 ; i < files.length ; i++){
			
			System.out.println(files[i]);
		}
		
		System.out.println("fin");
		
	}
}
