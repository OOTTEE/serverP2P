package es.uvigo.esei.dai.hybridserver.xmlserver.controllers;

import java.sql.SQLException;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;
import javax.swing.text.html.HTML;

import sun.security.x509.X400Address;
import es.uvigo.esei.dai.hybridserver.file.HTMLfile;
import es.uvigo.esei.dai.hybridserver.file.XMLfile;
import es.uvigo.esei.dai.hybridserver.file.XSDfile;
import es.uvigo.esei.dai.hybridserver.file.XSLTfile;

@WebService
@SOAPBinding(style=Style.RPC)
public interface ControllerInterface {
	/**
	 * Recupera la lista de ficheros HTML almacenados en el sevidor
	 * @return Array de uuids 
	 */
	@WebMethod public String[] getHTMLList();
	/**
	 * Recupera un archivo HTML dado por el uuid que lo identifica
	 * @param uuid uuid del HTML
	 * @return El archivo html que contienen el uuid y el content
	 */
	@WebMethod public HTMLfile getHTML(String uuid);

	/**
	 * Recupera la lista de ficheros XML almacenados en el sevidor
	 * @return Array de uuids 
	 */
	@WebMethod public String[] getXMlList();
	/**
	 * Recupera un archivo XML dado por el uuid que lo identifica
	 * @param uuid uuid del XML
	 * @return El archivo XML que contienen el uuid y el content
	 */
	@WebMethod public XMLfile getXML(String uuid);

	/**
	 * Recupera la lista de ficheros XSLT almacenados en el sevidor
	 * @return Array de uuids 
	 */
	@WebMethod public String[] getXSLTList();
	/**
	 * Recupera un archivo XSLT dado por el uuid que lo identifica
	 * @param uuid uuid del XSLT
	 * @return El archivo XSLT que contienen el uuid , el uuid del XSD asociado y el content
	 */
	@WebMethod public XSLTfile getXSLT(String uuid);

	/**
	 * Recupera la lista de ficheros XSD almacenados en el sevidor
	 * @return Array de uuids 
	 */
	@WebMethod public String[] getXSDList();
	/**
	 * Recupera un archivo XSD dado por el uuid que lo identifica
	 * @param uuid uuid del XSD
	 * @return El archivo html que contienen el uuid y el content
	 */
	@WebMethod public XSDfile getXSD(String uuid);
}
