package es.uvigo.esei.dai.hybridserver.enrutador.servicios;

import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebServiceException;

import es.uvigo.esei.dai.hybridserver.ServicioServer;

public class ServicesP2P {
	protected static ServicioServer serviceConnect(String URL, String namespaces, String serviceName) throws MalformedURLException, WebServiceException{
		
		Service service = Service.create(
				new URL(URL),
				new QName(
					namespaces,
					serviceName
				));

		return service.getPort(ServicioServer.class);
	}
	
}
