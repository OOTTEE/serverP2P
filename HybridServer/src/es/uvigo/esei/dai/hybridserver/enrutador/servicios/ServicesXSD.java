package es.uvigo.esei.dai.hybridserver.enrutador.servicios;

import java.net.MalformedURLException;
import java.util.Iterator;
import java.util.List;

import javax.xml.ws.WebServiceException;

import es.uvigo.esei.dai.hybridserver.ServerConfiguration;
import es.uvigo.esei.dai.hybridserver.file.XSDfile;

public class ServicesXSD extends ServicesP2P{

	public static XSDfile getPaginaServidorP2P(String uuid , List<ServerConfiguration> serverConfiguracion){

		if( serverConfiguracion != null){
			Iterator<ServerConfiguration> it = serverConfiguracion.iterator();
			
			XSDfile file = null;
			
			while(it.hasNext() && file == null){
				ServerConfiguration conf = it.next();
				try {
					file = serviceConnect(conf.getWsdl(), conf.getNamespace(), conf.getService()).getXSD(uuid);
				} catch (MalformedURLException | WebServiceException e) {
					System.err.println("Error al conectar con un servicio: " + e.getMessage());
				}
			}
			
			return file;
		}else{
			return null;
		}
		
	}
	
	public static String getListadoPaginasP2P(List<ServerConfiguration> serverConfiguracion){

		if( serverConfiguracion != null){
			String out = "<h1>P2P</h1>";
			
			Iterator<ServerConfiguration> it = serverConfiguracion.iterator();
			
			while(it.hasNext()){
				ServerConfiguration s = it.next();
				out += "<h2>"+s.getName()+"</h2>";
				try {
					out += getListadoPaginasServidorP2P(s);
				} catch (MalformedURLException e) {
					System.err.println("Error en la URl de un servicio:" + e.getMessage());
					out += "<p> Server not Conected </p>";
				} catch (Exception e) {
					System.err.println(e.getMessage());
					out += "<p> Server not Conected </p>";
				}
			}
			
			return out;
		}else{
			return null;
		}
	}
	
	private static String getListadoPaginasServidorP2P(ServerConfiguration conf) throws MalformedURLException, WebServiceException{
		String[] list = serviceConnect(conf.getWsdl(), conf.getNamespace(), conf.getService()).getXSDList();
		
		String out = "";
		
		for( int i = 0 ; i< list.length; i++)
			out += "<a href=\""+conf.getHttpAddress()+"xsd?uuid="+list[i]+"\">"+list[i]+"</a>" + "</br>\n";
		
		return out;
	}
}
