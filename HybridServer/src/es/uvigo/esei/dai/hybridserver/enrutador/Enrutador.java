package es.uvigo.esei.dai.hybridserver.enrutador;


import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import es.uvigo.esei.dai.hybridserver.ServerConfiguration;
import es.uvigo.esei.dai.hybridserver.dao.DAO;
import es.uvigo.esei.dai.hybridserver.dao.DBhtmlDAO;
import es.uvigo.esei.dai.hybridserver.dao.DBxmlDAO;
import es.uvigo.esei.dai.hybridserver.file.File;
import es.uvigo.esei.dai.hybridserver.file.HTMLfile;
import es.uvigo.esei.dai.hybridserver.http.HTTPRequest;
import es.uvigo.esei.dai.hybridserver.http.HTTPResponse;
import es.uvigo.esei.dai.hybridserver.http.HTTPResponseStatus;

/**
 * 
 * @author Javier Lorenzo Martin; Hector Nóvoa Nóvoa
 *
 */

public class Enrutador implements AutoCloseable{
	private Connection conexion;
	private List<ServerConfiguration> serverConfiguracion;
	
	public Enrutador(Connection conexion, List<ServerConfiguration> serverConfiguracion) {
		this.conexion = conexion;
		this.serverConfiguracion = serverConfiguracion;
	}

	public HTTPResponse process(HTTPRequest rq) {
		HTTPResponse rp = null; 
		if( rq.getResourceName().length() > 0){
			switch (rq.getResourceName()) {
			case "html":
				rp = (new SubEnrutadorHTML(this.conexion, this.serverConfiguracion)).process(rq);
				break;
			case "xml":
				rp = (new SubEnrutadorXML(this.conexion, this.serverConfiguracion)).process(rq);
				break;
			case "xsd":
				rp = (new SubEnrutadorXSD(this.conexion, this.serverConfiguracion)).process(rq);
				break;
			case "xslt":
				rp = (new SubEnrutadorXSLT(this.conexion, this.serverConfiguracion)).process(rq);
				break;
			default:
				rp = new HTTPResponse();
				rp.setStatus(HTTPResponseStatus.S400);
				break;
			}
		}else{
			rp = new HTTPResponse();
			rp.setContent("<html><body> <h1>"
					+ "<a href='/'>Hybrid Server</a></h1>"
					+ "<ul>"
						+ "<li><a href='/html'>HTML</a></li> "
						+ "<li><a href='/xml'>XML</a></li> "
						+ "<li><a href='/xsd'>XSD</a></li> "
						+ "<li><a href='/xslt'>XSLT</a></li>"
					+ "</ul>"
					+ "</body></html>");
		}
		return rp;
	}

	@Override
	public void close() throws SQLException {
		if(!this.conexion.isClosed()){
			this.conexion.close();			
		}
	}
}
