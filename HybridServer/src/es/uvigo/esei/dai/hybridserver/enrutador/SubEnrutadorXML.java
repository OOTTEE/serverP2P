package es.uvigo.esei.dai.hybridserver.enrutador;

import java.io.IOException;
import java.sql.Connection;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.ws.Response;

import org.xml.sax.SAXException;

import es.uvigo.esei.dai.hybridserver.ServerConfiguration;
import es.uvigo.esei.dai.hybridserver.dao.DAO;
import es.uvigo.esei.dai.hybridserver.dao.DBxmlDAO;
import es.uvigo.esei.dai.hybridserver.dao.DBxsdDAO;
import es.uvigo.esei.dai.hybridserver.dao.DBxsltDAO;
import es.uvigo.esei.dai.hybridserver.enrutador.servicios.ServicesP2P;
import es.uvigo.esei.dai.hybridserver.enrutador.servicios.ServicesXML;
import es.uvigo.esei.dai.hybridserver.enrutador.servicios.ServicesXSD;
import es.uvigo.esei.dai.hybridserver.enrutador.servicios.ServicesXSLT;
import es.uvigo.esei.dai.hybridserver.file.File;
import es.uvigo.esei.dai.hybridserver.file.XMLfile;
import es.uvigo.esei.dai.hybridserver.file.XSDfile;
import es.uvigo.esei.dai.hybridserver.file.XSLTfile;
import es.uvigo.esei.dai.hybridserver.http.HTTPRequest;
import es.uvigo.esei.dai.hybridserver.http.HTTPResponse;
import es.uvigo.esei.dai.hybridserver.http.HTTPResponseStatus;
import es.uvigo.esei.dai.hybridserver.utils.transformXSLT;

public class SubEnrutadorXML {
	final private String formInsert = "<form method=\"POST\" action=\"/xml\">	<input type=\"text\" name=\"xml\" id=\"xml\" />	<input type=\"submit\" name=\"submit\" value=\"enviar\"/> </form>";
	final private String title = "<h1><a href='/'>Hybrid Server</a></h1>";
	private DAO<XMLfile> bibliotecaXML;
	private DAO<XSLTfile>  bibliotecaXSLT;
	private DAO<XSDfile>  bibliotecaXSD;
	private List<ServerConfiguration> serverConfiguracion;
	/**
	 * Constructor
	 * @param biblioteca DAO Acceso a la base de información
	 */
	public SubEnrutadorXML(DAO<XMLfile> biblioteca){
		this.bibliotecaXML = biblioteca;		
	}

	/**
	 * Constructor
	 * @param bibliotecaXML informacion de conexion a la base de informacion
	 */
	public SubEnrutadorXML(Connection conexion, List<ServerConfiguration> serverConfiguration) {
		this.serverConfiguracion = serverConfiguration;
		this.bibliotecaXML = new DBxmlDAO(conexion);
		this.bibliotecaXSLT = new DBxsltDAO(conexion);
		this.bibliotecaXSD = new DBxsdDAO(conexion);
	}
		
	/**
	 * Process recibe una cabecera HTTP (Request), tras procesarla y verificar que sea correcta devuelve una respuesta HTTP (Response)
	 * @param rq Cabecera HTTP
	 * @return Respuesta HTTP
	 */
	public HTTPResponse process(HTTPRequest rq) {
		HTTPResponse rp = new HTTPResponse();
		rp.putParameter("Content-Type", "text/html");
		switch (rq.getMethod()) {
			case GET:
				get( rp, rq);
				break;
			case POST:		
				post( rp, rq);
				break;
			case DELETE:
				delete( rp, rq);
				break;
			default :
				System.out.println(this.title + "Metodo no implementado");
				break;
		}
		return rp;
	}
	/**
	 * Procesado de la cabecera con metodo GET
	 * @param rp HTTP Request
	 * @param rq HTTP Response
	 */
	private void get(HTTPResponse rp,HTTPRequest rq){
		if(rq.getResourceParameters().containsKey("uuid")){
			XMLfile  XML;
			if((XML = getXML(rq.getResourceParameters().get("uuid"))) != null){
				//Recurso encontrado url: .../xml?uuid="uuid existente en la biblioteca o en algun servidor de la red P2P

				//Si se solicita un XMl con un XSLT asociado
				if(rq.getResourceParameters().containsKey("xslt")){
					XSLTfile  XSLT;
					//Si el XSLT asociado existe en la base de datos o en algun servidor de la red P2P.
					if((XSLT = getXSLT(rq.getResourceParameters().get("xslt"))) != null){
						transformar(rp,XML,XSLT);
					}else{
						//Si no existe el XSLT  Not Found
						rp.setStatus(HTTPResponseStatus.S404);
					}	
				}else{
					//Si No nos solicitan un XSLT
					rp.setStatus(HTTPResponseStatus.S200);
					rp.putParameter("Content-Type", "application/xml");
					rp.setContent(((File) XML).getContent());
				}
			}else{
				rp.setStatus(HTTPResponseStatus.S404);
			}
		}else{
			//Recuperar el listado de las paginas url .../xml
			rp.setStatus(HTTPResponseStatus.S200);
			if(!this.bibliotecaXML.isEmpty()){
				String out=this.title + "<h2>Local Server</h2></body></xml>";

				out += this.getListadoPaginasServer();
				if(this.serverConfiguracion != null)
					out += ServicesXML.getListadoPaginasP2P(this.serverConfiguracion);
				
				out += formInsert;
				rp.setContent(out);
			}else{
				rp.setContent("No hay archivos</br>" + formInsert);
			}
		}
	}
	

	/**
	 * Procesado de la cabecera con metodo POST
	 * @param rp HTTP Request
	 * @param rq HTTP Response
	 */
	private void post (HTTPResponse rp,HTTPRequest rq){
		if(rq.getResourceParameters().containsKey("xml")){	
			rp.setStatus(HTTPResponseStatus.S200);
			File pagina = (File) this.bibliotecaXML.create(rq.getResourceParameters().get("xml"));
			rp.setContent("<a href=\"xml?uuid="+pagina.getUuid()+"\">"+pagina.getUuid()+"</a>");
		}else{
			rp.setStatus(HTTPResponseStatus.S400);
		}
	}

	/**
	 * Procesado de la cabecera con metodo DELETE
	 * @param rp HTTP Request
	 * @param rq HTTP Response
	 */
	private void delete(HTTPResponse rp,HTTPRequest rq){
		if (rq.getResourceParameters().containsKey("uuid")){
			if(this.bibliotecaXML.get(rq.getResourceParameters().get("uuid"))!= null){
				rp.setStatus(HTTPResponseStatus.S200);
				//Recurso encontrado url: .../xml?uuid="uuid existente en la biblioteca"
				this.bibliotecaXML.delete(rq.getResourceParameters().get("uuid"));
				rp.setContent("La Pagina se ha borrado correctamente");
			}else{
				//Recurso no encontrado url: .../xml?uuid="uuid no existende en la biblioteca"
				rp.setStatus(HTTPResponseStatus.S404);
			}
		}else{
			rp.setStatus(HTTPResponseStatus.S400);
		}
	}
	

	private String getListadoPaginasServer(){
		String out = "";
		for( XMLfile file : this.bibliotecaXML.list())
			out += "<a href=\"xml?uuid="+((File) file).getUuid()+"\">"+((File) file).getUuid()+"</a>" + "</br>\n";
		return out;
		
	}
	
	private void transformar(HTTPResponse rp, XMLfile XML, XSLTfile XSLT){
		XSDfile XSD;
		if((XSD = getXSD(XSLT.getXsd())) != null){
			//Si Existe el XSD en algun servidor de la red P2P o en la biblioteca 
			try {
				//Intentamos realizar la transformación, y tambien se comprueba que el XSD asociado al XSLT valide el XML
				rp.setContent(transformXSLT.transform(XML, XSLT, XSD));
				rp.putParameter("Content-Type", "text/html");
				rp.setStatus(HTTPResponseStatus.S200);
				System.out.print("200");
			} catch (ParserConfigurationException | SAXException | IOException e){
				//Si se produce una excepción durante la validación producimos un error Bad Request
				rp.setStatus(HTTPResponseStatus.S400);
			} catch ( TransformerException e) {
				//Si se produce un error durante la transformación producimo sun error 500;
				rp.setStatus(HTTPResponseStatus.S500);
			}
		}else{
			rp.setStatus(HTTPResponseStatus.S404);
		}
	}

	private XMLfile getXML( String uuid){
		XMLfile file;
		if((file = this.bibliotecaXML.get(uuid)) == null)
			file = ServicesXML.getPaginaServidorP2P(uuid, serverConfiguracion);
		return file;
	}
	
	private XSLTfile getXSLT( String uuid){
		
		XSLTfile file;
		if((file = this.bibliotecaXSLT.get(uuid)) == null)
			file = ServicesXSLT.getPaginaServidorP2P(uuid, serverConfiguracion);
		return file;
	}
	
	private XSDfile getXSD( String uuid){
		XSDfile file;
		if((file = this.bibliotecaXSD.get(uuid)) == null){
			file = ServicesXSD.getPaginaServidorP2P(uuid, serverConfiguracion);
		}
		return file;
	}
	


}
