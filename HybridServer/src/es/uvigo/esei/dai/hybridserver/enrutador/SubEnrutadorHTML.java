package es.uvigo.esei.dai.hybridserver.enrutador;

import java.sql.Connection;
import java.util.List;

import es.uvigo.esei.dai.hybridserver.ServerConfiguration;
import es.uvigo.esei.dai.hybridserver.dao.DAO;
import es.uvigo.esei.dai.hybridserver.dao.DBhtmlDAO;
import es.uvigo.esei.dai.hybridserver.enrutador.servicios.ServicesHTML;
import es.uvigo.esei.dai.hybridserver.file.File;
import es.uvigo.esei.dai.hybridserver.file.HTMLfile;
import es.uvigo.esei.dai.hybridserver.http.HTTPRequest;
import es.uvigo.esei.dai.hybridserver.http.HTTPResponse;
import es.uvigo.esei.dai.hybridserver.http.HTTPResponseStatus;

public class SubEnrutadorHTML {
	
	final private String formInsert = "<form method=\"POST\" action=\"/html\">	<input type=\"text\" name=\"html\" id=\"html\" />	<input type=\"submit\" name=\"submit\" value=\"enviar\"/> </form>";
	final private String title = "<h1><a href='/'>Hybrid Server</a></h1>";
	private DAO<HTMLfile> biblioteca;
	private List<ServerConfiguration> serverConfiguracion;
	/**
	 * Constructor
	 * @param biblioteca DAO Acceso a la base de información
	 */
	public SubEnrutadorHTML(DAO<HTMLfile> biblioteca) {
		this.biblioteca = biblioteca;		
	}

	/**
	 * Constructor
	 * @param biblioteca informacion de conexion a la base de informacion
	 */
	public SubEnrutadorHTML(Connection conexion, List<ServerConfiguration> serverConfiguration) {
		this.serverConfiguracion = serverConfiguration;
		this.biblioteca = new DBhtmlDAO(conexion);
	}
	
	/**
	 * Process recibe una cabecera HTTP (Request), tras procesarla y verificar que sea correcta devuelve una respuesta HTTP (Response)
	 * @param rq Cabecera HTTP
	 * @return Respuesta HTTP
	 */
	public HTTPResponse process(HTTPRequest rq) {
		HTTPResponse rp = new HTTPResponse();
		switch (rq.getMethod()) {
			case GET:
				get( rp, rq);
				break;
			case POST:		
				post( rp, rq);
				break;
			case DELETE:
				delete( rp, rq);
				break;
			default :
				System.out.println(this.title + "Metodo no implementado");
				break;
		}
		rp.putParameter("Content-Type", "text/html");
		return rp;
	}
	/**
	 * Procesado de la cabecera con metodo GET
	 * @param rp HTTP Request
	 * @param rq HTTP Response
	 */
	private void get(HTTPResponse rp,HTTPRequest rq){
		if(rq.getResourceParameters().containsKey("uuid")){
			if(this.biblioteca.get(rq.getResourceParameters().get("uuid"))!= null){
				//Recurso encontrado url: .../html?uuid="uuid existente en la biblioteca"
				rp.setStatus(HTTPResponseStatus.S200);
				rp.setContent(((File) this.biblioteca.get(rq.getResourceParameters().get("uuid"))).getContent());
			}else{
				//Recurso no encontrado url: .../html?uuid="uuid no existende en la biblioteca"
				File file;
					
				file = ServicesHTML.getPaginaServidorP2P(rq.getResourceParameters().get("uuid"),this.serverConfiguracion);
				if( file != null){
					rp.setContent(file.getContent());
					rp.setStatus(HTTPResponseStatus.S200);
				}else{
					rp.setStatus(HTTPResponseStatus.S404);
				}
				
			}
		}else{
			//Recuperar el listado de las paginas url .../html
			rp.setStatus(HTTPResponseStatus.S200);
			if(!this.biblioteca.isEmpty()){
				String out=this.title + "<h2>Local Server</h2></body></html>";
				
				out += this.getListadoPaginasServer();
				if(this.serverConfiguracion != null)
					out += ServicesHTML.getListadoPaginasP2P(this.serverConfiguracion);
				
				rp.setContent(out);
			}else{
				rp.setContent("No hay archivos</br>" + formInsert);
			}
		}
	}
	

	/**
	 * Procesado de la cabecera con metodo POST
	 * @param rp HTTP Request
	 * @param rq HTTP Response
	 */
	private void post (HTTPResponse rp,HTTPRequest rq){
		if(rq.getResourceParameters().containsKey("html")){	
			rp.setStatus(HTTPResponseStatus.S200);
			File pagina = (File) this.biblioteca.create(rq.getResourceParameters().get("html"));
			rp.setContent("<a href=\"html?uuid="+pagina.getUuid()+"\">"+pagina.getUuid()+"</a>");
		}else{
			rp.setStatus(HTTPResponseStatus.S400);
		}
	}

	/**
	 * Procesado de la cabecera con metodo DELETE
	 * @param rp HTTP Request
	 * @param rq HTTP Response
	 */
	private void delete(HTTPResponse rp,HTTPRequest rq){
		if (rq.getResourceParameters().containsKey("uuid")){
			if(this.biblioteca.get(rq.getResourceParameters().get("uuid"))!= null){
				rp.setStatus(HTTPResponseStatus.S200);
				//Recurso encontrado url: .../html?uuid="uuid existente en la biblioteca"
				this.biblioteca.delete(rq.getResourceParameters().get("uuid"));
				rp.setContent("La Pagina se ha borrado correctamente");
			}else{
				//Recurso no encontrado url: .../html?uuid="uuid no existende en la biblioteca"
				rp.setStatus(HTTPResponseStatus.S404);
			}
		}else{
			rp.setStatus(HTTPResponseStatus.S400);
		}
	}

	private String getListadoPaginasServer(){
		String out = "";
		for( HTMLfile file : this.biblioteca.list())
			out += "<a href=\"html?uuid="+((File) file).getUuid()+"\">"+((File) file).getUuid()+"</a>" + "</br>\n";
		return out;
	}
	
}
