/**
 *  HybridServer
 *  Copyright (C) 2014 Miguel Reboiro-Jato
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uvigo.esei.dai.hybridserver.http;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HTTPResponse {
	
	private HTTPResponseStatus status;
	private String HTTPversion;
	private Map<String, String> parameters;
	private String content;
	
	
	public HTTPResponse() {
		this.HTTPversion = HTTPHeaders.HTTP_1_1.getHeader();
		this.status = HTTPResponseStatus.S200;
		this.parameters = new HashMap<>();
	}
	
	public HTTPResponseStatus getStatus() {
		return status;
	}

	public void setStatus(HTTPResponseStatus status) {
		this.status = status; 
	}

	public String getVersion() {
		return HTTPversion;
	}

	public void setVersion(String version) {
		this.HTTPversion = version; 
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content=content;
		if(this.content != null && this.content.length() > 0)
			this.putParameter(HTTPHeaders.CONTENT_LENGTH.getHeader(), Integer.toString(this.content.length()));
	}

	public Map<String, String> getParameters() {
		return this.parameters;
	}
	
	public String putParameter(String name, String value) {
		return this.parameters.put(name, value);
	}
	
	public boolean containsParameter(String name) {
		return !this.parameters.isEmpty();
	}
	
	public String removeParameter(String name) {
		return this.parameters.remove(name);
	}
	
	public void clearParameters() {
		this.parameters.clear();
	}
	
	public List<String> listParameters() {
		List<String> aux = new ArrayList<String>();
		for (Map.Entry<String, String> param : this.parameters.entrySet())
			aux.add(param.getKey());
		return aux;
	}

	public void print(Writer writer) throws IOException {
			writer.write(this.HTTPversion + " " + this.status.getCode() + " " + this.status.getStatus() + "\n");
			for (Map.Entry<String, String> param : this.parameters.entrySet()) {
				writer.write(param.getKey() + ": " + param.getValue() + "\n");
			}
			writer.write("\n");
			if(this.content != null && this.content.length() > 0){
				writer.write(this.content);
			}
			writer.flush();
	}
	
	@Override
	public String toString() {
		final StringWriter writer = new StringWriter();
		
		try {
			this.print(writer);
		} catch (IOException e) {}
		
		return writer.toString();
	}
}
