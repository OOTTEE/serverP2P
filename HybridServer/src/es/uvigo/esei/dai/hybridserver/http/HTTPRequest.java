/**
 *  HybridServer
 *  Copyright (C) 2014 Miguel Reboiro-Jato
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uvigo.esei.dai.hybridserver.http;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.ListIterator;
import java.util.Map;



public class HTTPRequest {
	private BufferedReader reader;
	
	
	private HTTPRequestMethod method;				// GET , POST , ...*
	private String resourceChain; 					// /hello/world.html?country=Spain&province=Ourense&city=Ourense*
	private String resourceName;					// hello/world.html
	private Map<String, String> resourceParameter;  // map -> country-Spain, Province-Ourense....
	private HTTPHeaders HTTPversion;				// HTTP/1.1*
	private Map<String, String> headersParameters;	// map -> Host-localhost, Accept-text/html, ...
	private String[] resourcePath;					// ?
	private String content;							// Contenido despues del salto de linea
	private int contentLength;						// Longitud en caracteres del contenido content.length
	
	private ArrayList<String> packetLines;
	
	public HTTPRequest(Reader reader) throws IOException, HTTPParseException {
		this.reader = new BufferedReader(reader);
		this.resourceParameter = new LinkedHashMap<String,String>();
		this.headersParameters = new LinkedHashMap<String,String>();
		this.packetLines = new ArrayList<String>();
		String line;
		line = this.reader.readLine();
		while(line != null && !line.equals("")){
			this.packetLines.add(line);
			line = this.reader.readLine();
		}
		

		//en la primera linea se encuentran el metodo , la cadena del recurso y la version http separados por espacios
		//Ejemplo GET /..../index.php?value=5,value2=hola, HTTP/1.1

		String [] aux = ((String) (this.packetLines.toArray())[0]).split(" ",3);
		
		if(aux.length != 3){
			throw new HTTPParseException("Bad Header: Faltan parametros en la primera linea de la cabecera");
		}else{
				//METHOD Si el metodo no es valido lanzamos una excepcion
			for( HTTPRequestMethod x : HTTPRequestMethod.values()){
				if(x.name().equals(aux[0])){
					this.method=x;
					break;
				}
			}

			if(this.method==null){
				throw new HTTPParseException("Bad Method: Metodo " + aux[0] + " no reconocido");
			}

				//resourceChain Si la cadena no es valida lanzamos una excepcion
			if( aux[1].matches("/+[\\w:#@%/;$()\\[\\]~_?\\+-=.&]*") ){
				this.resourceChain=aux[1];
			}else
				throw new HTTPParseException("Bad resourceChain: " + aux[1] + " no es valido");
			
				//HTTPversion si la version no es valida lanzamos una excepcion
			if(HTTPHeaders.HTTP_1_1.getHeader().equals(aux[2])){
				this.HTTPversion = HTTPHeaders.HTTP_1_1;
			}else{
				throw new HTTPParseException("Bad resourceChain: " + aux[1] + "no es valido");
			}

		}
		//ResourceName
		if(resourceChain.matches("^/[^?]*(\\?.+)*")){
			aux=resourceChain.split("\\?",2);
			this.resourceName=aux[0].substring(1);
		}

		String x;
		//ResourceParameters Metodos no POST
		ListIterator<String>  par;
		String Rparametros=null;
		if(this.method != HTTPRequestMethod.POST ){
			Rparametros= new String((aux.length == 2 && aux[1]!=null)? aux[1] : "");

		}		
		
		//Mapeado de headersParameters
		
		par =  this.packetLines.subList(1,this.packetLines.toArray().length).listIterator();
		//recorremos el iterador hasta encontrar la linea en blanco que separa la cabecera del contenido

		
		x=par.next();
		while(!x.equals("")){
			if(!x.matches(".+: .+"))
				throw new HTTPParseException("Bad header parameter: " + x);
			this.headersParameters.put(x.split(": ")[0],x.split(": ")[1]);
			x= (par.hasNext()) ?par.next() : "";
		}
		
		if(this.headersParameters.containsKey(HTTPHeaders.CONTENT_LENGTH.getHeader())){
			this.contentLength = Integer.parseInt(this.headersParameters.get(HTTPHeaders.CONTENT_LENGTH.getHeader()));
		}
		
		char[] contenido = new char[this.contentLength];
		this.reader.read(contenido, 0, this.contentLength);
		this.content = new String(contenido);
		
		//Si el metodo es POST Recogemos los parametros
		if(this.method == HTTPRequestMethod.POST && this.content.length() > 0){
			Rparametros = new String(this.content);
		}

		if(this.headersParameters.containsKey(HTTPHeaders.CONTENT_TYPE.getHeader()) && 
				this.headersParameters.get(HTTPHeaders.CONTENT_TYPE.getHeader()).startsWith(MIME.FORM.getMime()) ){
			Rparametros = URLDecoder.decode(Rparametros,"UTF-8");
			this.content = URLDecoder.decode(this.content,"UTF-8");
		}

		
		if( Rparametros.matches("([^&]+=[^&]+)(&([^&]+=[^&]+))*")){
			par =  new ArrayList<String>(Arrays.asList( Rparametros.split("&") )).listIterator();
			while(par.hasNext()){
				x = par.next();
				this.resourceParameter.put(x.split("=")[0],x.split("=")[1]);
			}
		}else if(aux.length == 2 ){
			//Si la lista de parametros no tiene el formato correcto lanzamos excepcion
			throw new HTTPParseException("Bad list parameters POST format: "+ Rparametros);
		}	
		
	}
	
	
	public HTTPRequestMethod getMethod(){
		return this.method;
	}

	public String getResourceChain() {
		return this.resourceChain;
	}
	
	public String[] getResourcePath() {
		return this.resourcePath;
	}
	
	public String getResourceName() {
		return this.resourceName;
	}
	
	public Map<String, String> getResourceParameters() {
		return this.resourceParameter;
	}
	
	public String getHttpVersion() {
		return this.HTTPversion.getHeader();
	}

	public Map<String, String> getHeaderParameters() {
		return this.headersParameters;
	}

	public String getContent() {
		return (!this.content.equals("")) ? this.content : null;
	}
	
	public int getContentLength() {
		return  this.contentLength;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder(this.getMethod().name())
			.append(' ').append(this.getResourceChain())
			.append(' ').append(this.getHttpVersion())
		.append('\n');
		for (Map.Entry<String, String> param : this.getHeaderParameters().entrySet()) {
			sb.append(param.getKey()).append(": ").append(param.getValue()).append('\n');
		}
				
		if (this.contentLength > 0) {
			sb.append('\n').append(this.getContent());
		}
		
		return sb.toString();
	}
}
