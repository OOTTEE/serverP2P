package es.uvigo.esei.dai.hybridserver;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


/**
 * Listen Thread implementa la parte del servidor que escucha las peticiones y las asocia a hilos de procesado.
 * Para ello utiliza un Pool de hilos el cual se encarga de administrar y controlar la cola de peticiones.
 *  
 * @author Javier Lorenzo Martin; Hector Nóvoa Nóvoa
 *
 */
public class ListenThread extends Thread{
	
	private boolean running;
	private ServerSocket serverSocket;
	private Configuration configuration;
	private	ExecutorService piscinaDeHilos;

	/**
	 * Constructor
	 * @param port Puerto de escucha
	 * @param conexion objeto Conexion a la base de informacion
	 * @param numThreads Numero de hilos de la piscina de hilos
	 * @throws IOException
	 */
	public ListenThread(Configuration configuration) throws IOException  {
		this.running = false;
		this.configuration = configuration;
	}
	
	/**
	 * Para la ejecucion del servidor
	 * @throws IOException
	 */
	public void setStop() throws IOException{
		this.running = false;
		this.serverSocket.close();
		this.piscinaDeHilos.shutdown();
	}
	
	/**
	 * Hilo de ejecucion de ListenThread, Crea el socket de escucha y asigna las nuevas peticiones al pool de hilos.
	 */
	@Override
	public void run() {
		try(ServerSocket serverSocket = new ServerSocket(this.configuration.getHttpPort())) {
			this.serverSocket = serverSocket;
			this.running = true;
			this.piscinaDeHilos = Executors.newFixedThreadPool(this.configuration.getNumClients());
			while(this.running){
				piscinaDeHilos.execute(new ServidorThread(serverSocket.accept(), this.configuration));
			}	
		} catch (IOException e){
			System.err.println("ListenThread IOException: "+e.getMessage());
			//e.printStackTrace();
			
		}
	}

}
